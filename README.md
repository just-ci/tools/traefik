# Traefik

## Deploy

This assumes you've configured the daemon as a Docker Swarm manager. If you
haven't done so, just run `docker swarm init`.

1. Create a new GitLab project which can deploy the traefik instance using a
[privileged runner](https://gitlab.com/just-ci/tools/gitlab-runner).

2. Then add the following to a new file called `.gitlab-ci.yml` in your repo,
and set the email address for the default cert resolver.

```yaml
---
include:
  remote: https://gitlab.com/just-ci/tools/traefik/raw/v1.2.5/deploy.yml

variables:
  TRAEFIK_CERTIFICATESRESOLVERS_DEFAULT_ACME_EMAIL: change_me
```

Commit, push, and done!

## Adding containers

When deploying a new service, you need some extra labels in your
`docker-compose.yml` service definition, as well as an external network in
traefik and your service.

You can for example add the following to `docker-compose.yml` in the repo
of your service. This is an example of exposing the
[Traefik API](https://doc.traefik.io/traefik/operations/api/).

```yaml
services:
  my-app:
    networks:
      - default
      - proxy-my-app
    deploy:
      labels:
        - traefik.enable=true
        - traefik.http.routers.my-app.rule=Host(`my-app.example.com`)
        - traefik.http.services.my-app.loadbalancer.server.port=8080
    # and other stuff about your service

networks:
  proxy-my-app:
    external: true
```

> Swarm mode requires these labels under `deploy`, and it needs you to specify
the port.

Add that network to traefik as well, using a `docker-compose.override.yml` in
your traefik repo, as such:

```yaml
services:
  traefik:
    networks:
      - default
      - proxy-my-app

networks:
  proxy-my-app:
    external: true
```

> Networks will automatically be created if they don't exist using a `.pre` job.

## Customize

If you'd like to customize anything, add the fields you'd wish to add or
override to `docker-compose.override.yml`, `traefik.yml`, and/or
`traefikfile.yml` in your repo. `docker compose` and `yq` will ensure
these are merged, prioritizing your files.

### certificateResolver

If you wish to use a different certificateResolver challenge by default, be
sure to override the httpChallenge entry with `null`. For example:

`traefik.yml`
```yaml
certificatesResolvers:
  default:
    acme:
      email: your@email.address
      dnsChallenge:
        provider: transip
      httpChallenge: null
```

### Wildcard certificate

You can provide a wildcard certificate for all your containers by setting a
domain on the entrypoint:

`traefik.yml`
```yaml
entryPoints:
  https:
    http:
      tls:
        domains:
          - main: example.com
            sans:
              - "*.example.com"
```
