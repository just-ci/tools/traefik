## [1.2.5](https://gitlab.com/just-ci/tools/traefik/compare/v1.2.4...v1.2.5) (2024-08-26)


### Bug Fixes

* improve security headers ([2430f45](https://gitlab.com/just-ci/tools/traefik/commit/2430f45a47ef21dc48e4a87659919d441df43d4a))

## [1.2.4](https://gitlab.com/just-ci/tools/traefik/compare/v1.2.3...v1.2.4) (2024-07-21)


### Bug Fixes

* ci deploy update ([a446aaa](https://gitlab.com/just-ci/tools/traefik/commit/a446aaa067600dedfbaf395e497a9fecaca70246))

## [1.2.3](https://gitlab.com/just-ci/tools/traefik/compare/v1.2.2...v1.2.3) (2024-06-08)


### Bug Fixes

* single deploy job ([2996757](https://gitlab.com/just-ci/tools/traefik/commit/2996757b44cd9596b352c8248659c9e4aff83cd6))

## [1.2.2](https://gitlab.com/just-ci/tools/traefik/compare/v1.2.1...v1.2.2) (2024-05-07)


### Bug Fixes

* new ci config creation ([96dca31](https://gitlab.com/just-ci/tools/traefik/commit/96dca317264874871502e989f2b7b7d612b618a1))

## [1.2.1](https://gitlab.com/just-ci/tools/traefik/compare/v1.2.0...v1.2.1) (2024-05-07)


### Bug Fixes

* remove stack incompatible name field ([321cb0f](https://gitlab.com/just-ci/tools/traefik/commit/321cb0f3a415f833f134ce65f12008e99d8e7978))

# [1.2.0](https://gitlab.com/just-ci/tools/traefik/compare/v1.1.3...v1.2.0) (2024-05-07)


### Features

* expose http3 udp port ([c975835](https://gitlab.com/just-ci/tools/traefik/commit/c97583550d2cf2b8269ef344ee9bf7b248b5099e))

## [1.1.3](https://gitlab.com/just-ci/tools/traefik/compare/v1.1.2...v1.1.3) (2024-05-07)


### Bug Fixes

* fixed network ci ([dbc34ed](https://gitlab.com/just-ci/tools/traefik/commit/dbc34ed50c15b38251265e80aa3273e4cc28957a))

## [1.1.2](https://gitlab.com/just-ci/tools/traefik/compare/v1.1.1...v1.1.2) (2024-05-07)


### Bug Fixes

* download compose first ([3a34f11](https://gitlab.com/just-ci/tools/traefik/commit/3a34f1103e1463c382a55c62a4ba40d097b0b841))

## [1.1.1](https://gitlab.com/just-ci/tools/traefik/compare/v1.1.0...v1.1.1) (2024-05-07)


### Bug Fixes

* version pin warning ([c748159](https://gitlab.com/just-ci/tools/traefik/commit/c7481590b94cf872025232f53f79bc47197f3b9c))

# [1.1.0](https://gitlab.com/just-ci/tools/traefik/compare/v1.0.9...v1.1.0) (2024-05-07)


### Features

* use deploy template ([2bf2572](https://gitlab.com/just-ci/tools/traefik/commit/2bf2572f635e43d5597c9673f5034d357f150de6))

## [1.0.9](https://gitlab.com/just-ci/tools/traefik/compare/v1.0.8...v1.0.9) (2024-05-03)


### Bug Fixes

* better pinned version var ([bde2e30](https://gitlab.com/just-ci/tools/traefik/commit/bde2e30dbd4739591a1f84e6d0a8e272ed01230d))

## [1.0.8](https://gitlab.com/just-ci/tools/traefik/compare/v1.0.7...v1.0.8) (2023-12-21)


### Bug Fixes

* use stack config ([b5e3f80](https://gitlab.com/just-ci/tools/traefik/commit/b5e3f8036259e390e515357a622443fba30c5955))

## [1.0.7](https://gitlab.com/just-ci/tools/traefik/compare/v1.0.6...v1.0.7) (2023-04-21)


### Bug Fixes

* move entire config to pre job ([0c690cf](https://gitlab.com/just-ci/tools/traefik/commit/0c690cfe460e349cce821768c5e739d78442d9ce))

## [1.0.6](https://gitlab.com/just-ci/tools/traefik/compare/v1.0.5...v1.0.6) (2023-02-21)


### Bug Fixes

* remove default network name ([4a26660](https://gitlab.com/just-ci/tools/traefik/commit/4a2666035847a6f4842dac0f383fefaf9b5f65e5))

## [1.0.5](https://gitlab.com/just-ci/tools/traefik/compare/v1.0.4...v1.0.5) (2023-02-04)


### Bug Fixes

* dont hardcode x-frame-options ([415fcfc](https://gitlab.com/just-ci/tools/traefik/commit/415fcfc047f2fd26be1d3ab59f9dd383ab63c7ed))

## [1.0.4](https://gitlab.com/just-ci/tools/traefik/compare/v1.0.3...v1.0.4) (2023-01-29)


### Bug Fixes

* disable default CSP ([5a6c711](https://gitlab.com/just-ci/tools/traefik/commit/5a6c711e1bf312cfe22a4aa3f9d30e31868ec0b4))

## [1.0.3](https://gitlab.com/just-ci/tools/traefik/compare/v1.0.2...v1.0.3) (2023-01-02)


### Bug Fixes

* don't force robots tag header ([8469626](https://gitlab.com/just-ci/tools/traefik/commit/846962615e111363e096163acfe8fb9faced9b33))

## [1.0.2](https://gitlab.com/just-ci/tools/traefik/compare/v1.0.1...v1.0.2) (2022-11-21)


### Bug Fixes

* compose ouput ([a37f496](https://gitlab.com/just-ci/tools/traefik/commit/a37f496226edf31809bb97d9da788dd762a6c906))

## [1.0.1](https://gitlab.com/just-ci/tools/traefik/compare/v1.0.0...v1.0.1) (2022-11-21)


### Bug Fixes

* new location ([f660fb5](https://gitlab.com/just-ci/tools/traefik/commit/f660fb54cc4843476ce89c10cd1335266d88494b))

# 1.0.0 (2022-11-21)


### Bug Fixes

* add version ([355a0aa](https://gitlab.com/just-ci/tools/traefik/commit/355a0aa2859682aa15c8e56ba54df64424a55956))
* add yq in deploy ([60ea225](https://gitlab.com/just-ci/tools/traefik/commit/60ea22556b736c684fad1acecc94a81b8045f599))
* allow docker-compose.yml in root ([65adf61](https://gitlab.com/just-ci/tools/traefik/commit/65adf610722905974fd7e69894d4a6fb1d55d2e7))
* arg order ([b90d20e](https://gitlab.com/just-ci/tools/traefik/commit/b90d20e8a72a5848b8ad44872ede7184afff5f86))
* config port int bug ([b84d320](https://gitlab.com/just-ci/tools/traefik/commit/b84d32001abd722a7ae52a6158791663030c455d))
* keep env vars ([cb86235](https://gitlab.com/just-ci/tools/traefik/commit/cb862352fb140f8d9f2a3a55da7ce4ff24952372))
* simpler deploy, incl. updater ([ad15119](https://gitlab.com/just-ci/tools/traefik/commit/ad1511932d39337ddafc6437ec622ecda7e6c043))
* update to 2.9 ([08fcc1a](https://gitlab.com/just-ci/tools/traefik/commit/08fcc1ab7e539f1b27bf9748bd06bc556d845a19))
* use official docker image ([bf7c8f7](https://gitlab.com/just-ci/tools/traefik/commit/bf7c8f77f709ea6652551867f2f48ced8852b067))


### Features

* pull latest images first ([c7d8f73](https://gitlab.com/just-ci/tools/traefik/commit/c7d8f73f06916fc8ac3446be02337d7e439c0292))
